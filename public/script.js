function arrastrarElemento(elementoDePlaza) {
    //establecer 4 posiciones para posicionar en la pantalla
    let pos1 = 0,
     pos2 = 0,
     pos3 = 0,
     pos4 = 0;
    elementoDePlaza.onpointerdown = arrastrarPuntero;

    function arrastrarPuntero(e) {
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        posXInitial = e.clientX;
        posYInitial = e.clientY;
        console.log(posXInitial)
        document.onpointermove = arrastrarElemento;
        document.onpointerup = validarPosicion;
    }

    function arrastrarElemento(e) {
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elementoDePlaza.style.top = elementoDePlaza.offsetTop - pos2 + 'px';
        elementoDePlaza.style.left = elementoDePlaza.offsetLeft - pos1 + 'px';
    }

    function validarPosicion(){
        // verificar si la clase del elemento es un componente-cielo
        if(elementoDePlaza.className === 'componente-cielo'){
            // verificar que el elemento este dentro de las dimensiones del componente cielo
            if(cielo.getBoundingClientRect().top <= elementoDePlaza.getBoundingClientRect().top 
            && cielo.getBoundingClientRect().bottom >= elementoDePlaza.getBoundingClientRect().bottom
            && cielo.getBoundingClientRect().left <= elementoDePlaza.getBoundingClientRect().left 
            && cielo.getBoundingClientRect().right >= elementoDePlaza.getBoundingClientRect().right){
                console.log('ok')
            }else{
                // emitir un mensaje de alerta
                alert('Coloque en el cielo')

                // volver a su posicion original
                elementoDePlaza.style.left =0 + 'px';
                elementoDePlaza.style.top = 0 + 'px';
            }
        }
        // verificar si la clase del elemento es un componente-suelo
        if(elementoDePlaza.className === 'componente-suelo'){
            // verificar que el elemento este dentro de las dimensiones del componente suelo
            if(suelo.getBoundingClientRect().top <= elementoDePlaza.getBoundingClientRect().top 
            && suelo.getBoundingClientRect().bottom >= elementoDePlaza.getBoundingClientRect().bottom
            && suelo.getBoundingClientRect().left <= elementoDePlaza.getBoundingClientRect().left 
            && suelo.getBoundingClientRect().right >= elementoDePlaza.getBoundingClientRect().right){
                console.log('ok')
            }else{
                // emitir un mensaje de alerta
                alert('Coloque en la tierra')

                // volver a su posicion original
                elementoDePlaza.style.left =0 + 'px';
                elementoDePlaza.style.top = 0 + 'px';
            }
        }
        detenerArrastreElemento()
    }

}

function detenerArrastreElemento() {
    document.onpointerup = null;
    document.onpointermove = null;
}



arrastrarElemento(document.getElementById('plaza1'));
arrastrarElemento(document.getElementById('plaza2'));
arrastrarElemento(document.getElementById('plaza3'));
arrastrarElemento(document.getElementById('plaza4'));
arrastrarElemento(document.getElementById('plaza5'));
arrastrarElemento(document.getElementById('plaza6'));
arrastrarElemento(document.getElementById('plaza7'));
arrastrarElemento(document.getElementById('plaza8'));
arrastrarElemento(document.getElementById('plaza9'));
arrastrarElemento(document.getElementById('plaza10'));
arrastrarElemento(document.getElementById('plaza11'));
arrastrarElemento(document.getElementById('plaza12'));
arrastrarElemento(document.getElementById('plaza13'));
arrastrarElemento(document.getElementById('plaza14'));
arrastrarElemento(document.getElementById('plaza15'));
arrastrarElemento(document.getElementById('plaza16'));
arrastrarElemento(document.getElementById('plaza17'));
arrastrarElemento(document.getElementById('plaza18'));


cielo = document.getElementById('cielo')
suelo = document.getElementById('suelo')

document.onclick = show

function show(e){
    console.log(e.clientX)
}